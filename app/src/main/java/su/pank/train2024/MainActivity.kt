package su.pank.train2024

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ErrorOutline
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import org.koin.compose.koinInject
import su.pank.train2024.ui.navigation.GeneralNav
import su.pank.train2024.ui.theme.Train2024Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Train2024Theme {

                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val errorManager = koinInject<ErrorManager>()
                    val error by errorManager.error.collectAsState()

                    if (error != null) {
                        AlertDialog(
                            onDismissRequest = { errorManager.skipError() },
                            confirmButton = {
                                TextButton(
                                    onClick = { errorManager.skipError() }) {
                                    Text("OK")
                                }
                            }, title = { Text("Error") }, icon = {
                                Icon(
                                    Icons.Default.ErrorOutline,
                                    contentDescription = null
                                )
                            }, text = { Text(text = error ?: "") })
                    }

                    GeneralNav()
                }
            }
        }
    }
}

