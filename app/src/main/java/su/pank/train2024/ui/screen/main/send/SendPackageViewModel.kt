package su.pank.train2024.ui.screen.main.send

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.postgrest.from
import kotlinx.coroutines.launch
import su.pank.train2024.ErrorManager
import su.pank.train2024.data.model.DeliveryItemDTO
import su.pank.train2024.data.model.LatLng
import su.pank.train2024.data.model.PackageDetailsDTO
import su.pank.train2024.ui.models.PackageDescription
import su.pank.train2024.ui.models.PositionDescription

class SendPackageViewModel(private val client: SupabaseClient, private val errorManager: ErrorManager) : ViewModel() {
    var origin: PositionDescription = PositionDescription("", "", "", "")

    var dests = mutableStateListOf(PositionDescription("", "", "", ""))

    var packageDescription: PackageDescription = PackageDescription("", 0f, 0f)

    fun checkPoles() = origin.check() && dests.all { it.check() } && packageDescription.check()

    val deliveryCharges
        get() = 2500 * dests.size

    val instantDelivery = 300

    val tax
        get() = 0.05 * (deliveryCharges + instantDelivery)

    val sum
        get() = deliveryCharges + instantDelivery + tax


    fun makePayment(navController: NavController) {
        viewModelScope.launch {
            try {
                client.from("delivery").insert(
                    DeliveryItemDTO(
                        profile_id = client.auth.currentUserOrNull()?.id.toString(),
                        package_details = PackageDetailsDTO(origin, dests, packageDescription),
                        origin = LatLng(0.0, 0.0)
                    )
                )
                navController.navigate("Transaction")
            }catch (e: Exception){
                errorManager.addError(e)
            }

        }
    }
}