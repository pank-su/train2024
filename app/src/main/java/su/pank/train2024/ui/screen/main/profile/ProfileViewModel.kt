package su.pank.train2024.ui.screen.main.profile

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import su.pank.train2024.data.DarkModeRepository
import su.pank.train2024.domain.ProfileUseCase

class ProfileViewModel(profileUseCase: ProfileUseCase, private val darkModeRepository: DarkModeRepository) : ViewModel() {

    var image by mutableStateOf("")
    var name by mutableStateOf("")
    var balance by mutableIntStateOf(0)
    var isDark = darkModeRepository.isDark

    init {
        CoroutineScope(Dispatchers.IO).launch {
            val profile = profileUseCase.invoke()
            image = profile?.image ?: ""
            name = profile?.fullname ?: ""
            balance = profile?.balance ?: 0
        }
    }

    fun setIsDark(enabled: Boolean){
        CoroutineScope(Dispatchers.IO).launch {
            darkModeRepository.setDarkMode(enabled)
        }
    }
}