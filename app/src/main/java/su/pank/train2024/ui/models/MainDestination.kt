package su.pank.train2024.ui.models

import androidx.compose.runtime.Composable
import androidx.navigation.NavController

data class MainDestination(
    val name: String,
    val icon:  @Composable (Boolean) -> Unit,
    val content: @Composable (NavController) -> Unit
)
