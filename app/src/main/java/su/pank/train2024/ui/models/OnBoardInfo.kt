package su.pank.train2024.ui.models

import androidx.annotation.DrawableRes

data class OnBoardInfo(@DrawableRes val image: Int, val title: String, val description: String)
