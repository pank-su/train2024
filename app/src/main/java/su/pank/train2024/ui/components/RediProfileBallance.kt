package su.pank.train2024.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import io.eyram.iconsax.IconSax
import io.github.alexzhirkevich.cupertino.Surface
import io.github.jan.supabase.storage.publicStorageItem
import su.pank.train2024.ui.theme.Train2024Theme

@Composable
fun RediProfileBallance(imageName: String, userName: String, userBallance: Float) {

    var balanceShowed by remember {
        mutableStateOf(false)
    }
    Row(
        Modifier.padding(horizontal = 10.dp, vertical = 15.dp)
            .height(60.dp)
            ) {
        AsyncImage(
            model = publicStorageItem("avatars", imageName),
            contentDescription = null,
            modifier = Modifier
                .clip(CircleShape)
                .size(60.dp),
            contentScale = ContentScale.Crop

        )
        Spacer(modifier = Modifier.width(5.dp))
        Row(
            modifier = Modifier.fillMaxSize(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(modifier = Modifier) {
                Text(text = "Hello $userName", fontWeight = FontWeight.W500)
                Text(text = buildAnnotatedString {
                    append("Current balance")
                    withStyle(SpanStyle(MaterialTheme.colorScheme.primary)) {
                        if (balanceShowed)
                            append("N$userBallance")
                        else
                            append("N*********")
                    }
                }, style = MaterialTheme.typography.bodySmall)
            }
            IconButton(onClick = { balanceShowed = !balanceShowed }) {
                Icon(
                    painterResource(id = IconSax.Linear.EyeSlash),
                    contentDescription = null,
                    Modifier.size(14.dp)
                )
            }
        }


    }

}


@Preview
@Composable
private fun PreviewRediProfileBallance() {
    Train2024Theme {
        Surface {
            RediProfileBallance("test.jpg", "Vasya", 1000.00f)
        }
    }
}