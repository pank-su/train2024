package su.pank.train2024.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.github.jan.supabase.annotations.SupabaseExperimental
import io.github.jan.supabase.compose.auth.ui.email.OutlinedEmailField
import io.github.jan.supabase.compose.auth.ui.password.OutlinedPasswordField
import io.github.jan.supabase.compose.auth.ui.password.PasswordRule
import io.github.jan.supabase.compose.auth.ui.phone.OutlinedPhoneField
import su.pank.train2024.ui.theme.Train2024Theme

@Composable
private fun RediFieldLayout(
    label: String,
    error: String = "",
    modifier: Modifier = Modifier,
    isErrorVisible: Boolean = true,
    content: @Composable (Boolean) -> Unit,
) {
    val isError by remember {
        derivedStateOf {
            error.isNotBlank()
        }
    }

    LaunchedEffect(error) {
        println(isError)
    }

    Column(modifier, verticalArrangement = Arrangement.spacedBy(8.dp)) {
        Text(
            text = label,
            color = MaterialTheme.colorScheme.outline,
            style = MaterialTheme.typography.bodyMedium
        )
        content(isError)
        if (isErrorVisible)
            Text(
                text = error,
                color = if (!isError) Color.Transparent else MaterialTheme.colorScheme.error
            )
        if (!isErrorVisible)
            Spacer(modifier = Modifier.height(8.dp))
    }
}

@Composable
fun RediField(
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    placeholder: String,
    modifier: Modifier = Modifier,
    error: String = ""
) {
    RediFieldLayout(label = label, error = error, modifier = modifier) {
        OutlinedTextField(
            value = value,
            onValueChange = onValueChange,
            modifier = Modifier.fillMaxWidth(),

            isError = it,
            placeholder = {
                Text(
                    text = placeholder
                )
            })
    }
}

@OptIn(SupabaseExperimental::class, ExperimentalMaterial3Api::class)
@Composable
fun RediEmailField(
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    placeholder: String,
    modifier: Modifier = Modifier,
) {
    RediFieldLayout(label = label, modifier = modifier, isErrorVisible = false) {
        OutlinedEmailField(
            value = value,
            onValueChange = onValueChange,
            modifier = Modifier.fillMaxWidth(),

            leadingIcon = null,
            placeholder = {
                Text(
                    text = placeholder
                )
            })
    }
}

@OptIn(SupabaseExperimental::class, ExperimentalMaterial3Api::class)
@Composable
fun RediPhoneField(
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    placeholder: String,
    modifier: Modifier = Modifier,
) {
    RediFieldLayout(label = label, modifier = modifier, isErrorVisible = false) {
        OutlinedPhoneField(
            value = value,
            onValueChange = onValueChange,
            modifier = Modifier.fillMaxWidth(),

            leadingIcon = null,
            placeholder = {
                Text(
                    text = placeholder
                )
            })
    }
}

@OptIn(SupabaseExperimental::class, ExperimentalMaterial3Api::class)
@Composable
fun RediSecureField(
    value: String,
    onValueChange: (String) -> Unit,
    label: String,
    placeholder: String,
    modifier: Modifier = Modifier,
    rules: List<PasswordRule> = listOf()
) {
    RediFieldLayout(label = label, modifier = modifier, isErrorVisible = false) {
        OutlinedPasswordField(
            value = value,
            onValueChange = onValueChange,
            modifier = Modifier.fillMaxWidth(),
            leadingIcon = null,
            placeholder = {
                Text(
                    text = placeholder
                )
            }, rules = rules
        )
    }
}


@Preview(showBackground = true)
@Composable
fun PreviewRediField() {
    Train2024Theme {
        var value by remember {
            mutableStateOf("")
        }
        RediSecureField(
            value,
            { value = it },
            "Full name",
            placeholder = "Ivanov Ivan",
            rules = listOf(PasswordRule.minLength(8))
        )


    }
}