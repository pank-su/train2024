package su.pank.train2024.ui.screen.auth.forgot

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import io.github.jan.supabase.annotations.SupabaseExperimental
import io.github.jan.supabase.compose.auth.ui.AuthForm
import io.github.jan.supabase.compose.auth.ui.LocalAuthState
import org.koin.androidx.compose.koinViewModel
import su.pank.train2024.ui.components.RediButton
import su.pank.train2024.ui.components.RediEmailField
import su.pank.train2024.ui.components.RediTitle

@OptIn(SupabaseExperimental::class)
@Composable
fun ForgotPasswordScreen(modifier: Modifier = Modifier, navController: NavController, vm: ForgotPasswordViewModel = koinViewModel()) {
    AuthForm {
        Column(
            modifier = Modifier
                .padding(start = 24.dp, end = 24.dp)

        ) {
            Spacer(modifier = Modifier.height(110.dp))

            RediTitle(
                title = "Forgot Password",
                description = "Enter your email address"
            )

            Spacer(modifier = Modifier.height(56.dp))
            RediEmailField(
                value = vm.email,
                onValueChange = { vm.email = it },
                label = "Email Address",
                placeholder = "***********@mail.com"
            )
            val authState = LocalAuthState.current
            RediButton(
                onClick = { vm.sendOTP(navController) },
                modifier = Modifier.fillMaxWidth(),
                enabled = authState.validForm
            ) {
                Text(text = "Send OTP", fontSize = 16.sp, fontWeight = FontWeight.W700)
            }
            val annotatedText = buildAnnotatedString {
                append("Remember password? Back to ")
                pushStringAnnotation(
                    tag = "URL", annotation = "https://developer.android.com"
                )
                withStyle(
                    style = SpanStyle(
                        color = MaterialTheme.colorScheme.primary
                    )
                ) {
                    append("Sign in")
                }
                pop()
            }
            Spacer(modifier = Modifier.height(20.dp))
            ClickableText(
                text = annotatedText,
                modifier = Modifier.fillMaxWidth(),
                onClick = { offset ->
                    annotatedText.getStringAnnotations(
                        tag = "URL", start = offset, end = offset
                    ).firstOrNull()?.let {
                        navController.navigate("SignUpScreen")
                    }
                },
                style = MaterialTheme.typography.bodyMedium.copy(
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colorScheme.outline
                )
            )
        }
    }
}