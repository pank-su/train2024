package su.pank.train2024.ui.screen.main.send

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import kotlinx.coroutines.delay
import su.pank.train2024.R
import su.pank.train2024.ui.components.RediButton
import su.pank.train2024.ui.components.RediOutlinedButton

@Composable
fun TransactionScreen(navController: NavHostController) {
    var isComplete by remember {
        mutableStateOf(false)
    }
    LaunchedEffect(key1 = null) {
        delay(3000)
        isComplete = true
    }
    Column(
        Modifier.padding(top = 99.dp, start = 24.dp, end = 24.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AnimatedVisibility(visible = !isComplete) {
            CircularProgressIndicator(
                modifier = Modifier.size(119.dp),
                color = MaterialTheme.colorScheme.secondary
            )
        }
        AnimatedVisibility(visible = isComplete) {
            Image(
                painter = painterResource(id = R.drawable.good_tick),
                contentDescription = null,
                modifier = Modifier.size(119.dp),
            )
        }
        Spacer(modifier = Modifier.height(141.dp))
        Text(
            text = "Transaction Successful",
            style = MaterialTheme.typography.headlineSmall,
            fontWeight = FontWeight.W500,
            color = if (!isComplete) Color.Transparent else MaterialTheme.colorScheme.onSurface
        )
        Text(text = "Your rider is on the way to your destination")
        Text(text = buildAnnotatedString {
            append("Tracking Number ")
            withStyle(SpanStyle(MaterialTheme.colorScheme.primary)) {
                append("R-7458-4567-4434-5854")
            }
        })
        Spacer(modifier = Modifier.height(98.dp))

        Column {
            RediButton(onClick = { navController.navigate("Track") }, modifier = Modifier.fillMaxWidth()) {
                Text(text = "Track my item", fontSize = 16.sp, fontWeight = FontWeight.W700)
            }
        }
        Spacer(modifier = Modifier.height(8.dp))

        Column {
            RediOutlinedButton(onClick = { navController.navigate("Home") }, modifier = Modifier.fillMaxWidth()) {
                Text(text = "Go back to homepage", fontSize = 16.sp, fontWeight = FontWeight.W700)
            }
        }


    }
}

@Preview
@Composable
private fun PreviewTransactionScreen() {
}