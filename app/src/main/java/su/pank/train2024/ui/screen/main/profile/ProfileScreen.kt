package su.pank.train2024.ui.screen.main.profile

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowForwardIos
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import io.eyram.iconsax.IconSax
import io.github.alexzhirkevich.cupertino.CupertinoScaffold
import io.github.alexzhirkevich.cupertino.CupertinoSwitch
import io.github.alexzhirkevich.cupertino.CupertinoSwitchDefaults
import io.github.alexzhirkevich.cupertino.CupertinoText
import io.github.alexzhirkevich.cupertino.CupertinoTopAppBar
import io.github.alexzhirkevich.cupertino.ExperimentalCupertinoApi
import io.github.alexzhirkevich.cupertino.Surface
import org.koin.androidx.compose.koinViewModel
import su.pank.train2024.R
import su.pank.train2024.ui.components.RediProfileBallance
import su.pank.train2024.ui.models.ProfileAction
import su.pank.train2024.ui.theme.Train2024Theme

@OptIn(ExperimentalCupertinoApi::class)
@Composable
fun ProfileScreen(navController: NavController) {
    val vm: ProfileViewModel = koinViewModel()
    val isDark by vm.isDark.collectAsState(false)
    CupertinoScaffold(topBar = {
        CupertinoTopAppBar(title = {
            CupertinoText(
                text = "Profile",
                color = MaterialTheme.colorScheme.outline
            )
        }, isTranslucent = false, modifier = Modifier.shadow(3.dp))
    }) {
        val items = listOf(
            ProfileAction(
                "Edit Profile",
                "Name, phone no, address, email ...",
                IconSax.Linear.ProfileCircle
            ) {},
            ProfileAction(
                "Statements & Reports",
                "Download transaction details, orders, deliveries",
                R.drawable.paper
            ) {},
            ProfileAction(
                "Notification Settings",
                "mute, unmute, set location & tracking setting",
                IconSax.Linear.Notification
            ) {
              navController.navigate("Notification")
            },

            ProfileAction(
                "Card & Bank account settings",
                "change cards, delete card details",
                IconSax.Linear.Wallet2
            ) {},
            ProfileAction(
                "Referrals",
                "check no of friends and earn",
                R.drawable.two_person_lift
            ) {},
            ProfileAction(
                "About Us",
                "know more about us, terms and conditions",
                R.drawable.map
            ) {},
            ProfileAction(
                "Log Out",
                "",
                R.drawable.log_out,
                color = MaterialTheme.colorScheme.error
            ) {}
        )
        LazyColumn(modifier = Modifier.padding(it)) {
            item {
                RediProfileBallance(imageName = vm.image, userName = vm.name, userBallance = 1000f)
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(64.dp)
                        .padding(horizontal = 24.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = "Enable dark Mode",
                        style = MaterialTheme.typography.bodyLarge,
                        fontWeight = FontWeight.W500
                    )
                    CupertinoSwitch(
                        checked = isDark,
                        modifier = Modifier.graphicsLayer {
                            scaleY = 0.8f
                            scaleX = 0.9f
                        },
                        onCheckedChange = {
                            vm.setIsDark(it)
                        },
                        colors = CupertinoSwitchDefaults.colors(checkedTrackColor = MaterialTheme.colorScheme.primary)
                    )
                }
            }
            items(items) {
                Card(
                    shape = RectangleShape,
                    modifier = Modifier
                        .padding(horizontal = 25.dp, vertical = 12.dp)
                        .shadow(2.dp)
                        .clickable {
                            it.onClick()
                        },
                    colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.surface)
                ) {
                    Row(
                        modifier = Modifier
                            .padding(12.dp), verticalAlignment = Alignment.CenterVertically
                    ) {
                        Icon(
                            painter = painterResource(id = it.icon),
                            null,
                            tint = it.color ?: MaterialTheme.colorScheme.onSurface
                        )
                        Spacer(modifier = Modifier.width(9.dp))
                        Row(
                            Modifier.fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            Column {
                                Text(text = it.title, fontWeight = FontWeight.W500)
                                if (it.description.isNotBlank()) {
                                    Text(
                                        text = it.description,
                                        style = MaterialTheme.typography.bodySmall,
                                        color = MaterialTheme.colorScheme.outline
                                    )
                                }
                            }
                            Icon(
                                Icons.AutoMirrored.Filled.ArrowForwardIos,
                                contentDescription = null,
                                Modifier.size(10.dp),
                            )
                        }

                    }
                }
            }
        }
    }
}

@Preview
@Composable
private fun PreviewProfile() {
    Train2024Theme {
        Surface {
            ProfileScreen(rememberNavController())
        }
    }
}