package su.pank.train2024.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import org.koin.androidx.compose.koinViewModel
import su.pank.train2024.ui.models.MainDestination
import su.pank.train2024.ui.screen.main.NotificationScreen
import su.pank.train2024.ui.screen.main.send.SendPackageCheckScreen
import su.pank.train2024.ui.screen.main.send.SendPackagePrepare
import su.pank.train2024.ui.screen.main.send.SendPackageViewModel
import su.pank.train2024.ui.screen.main.send.TransactionScreen


@Composable
fun MainNav(
    navController: NavHostController,
    parentNavController: NavController,
    modifier: Modifier = Modifier,
    dests: List<MainDestination>,
) {
    val sendPackageVm: SendPackageViewModel = koinViewModel()
    NavHost(navController = navController, startDestination = dests.first().name, modifier) {
        for (dest in dests) {
            composable(dest.name) {
                dest.content(navController)
            }
        }
        composable("Add Payment method") {

        }

        composable("Notification") {
            NotificationScreen()
        }
        composable("Send a package") {
            SendPackagePrepare(navController, sendPackageVm)
        }
        composable("Send a package ") {
            SendPackageCheckScreen(navController, sendPackageVm)
        }
        composable("Transaction"){
            TransactionScreen(navController)
        }
        composable("Tracking"){

        }

    }
}