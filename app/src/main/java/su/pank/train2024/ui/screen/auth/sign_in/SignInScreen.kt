package su.pank.train2024.ui.screen.auth.sign_in

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import io.github.jan.supabase.annotations.SupabaseExperimental
import io.github.jan.supabase.compose.auth.ui.AuthForm
import io.github.jan.supabase.compose.auth.ui.LocalAuthState
import org.koin.androidx.compose.koinViewModel
import su.pank.train2024.R
import su.pank.train2024.ui.components.RediButton
import su.pank.train2024.ui.components.RediEmailField
import su.pank.train2024.ui.components.RediSecureField
import su.pank.train2024.ui.components.RediTitle
import su.pank.train2024.ui.theme.Train2024Theme

@OptIn(SupabaseExperimental::class)
@Composable
fun SignInScreen(navController: NavController) {
    val scrollState = rememberScrollState()
    val vm: SignInViewModel = koinViewModel()

    Box(modifier = Modifier.fillMaxSize()) {
        AuthForm {
            Column(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)

            ) {
                Spacer(modifier = Modifier.height(110.dp))

                RediTitle(
                    title = "Welcome Back",
                    description = "Fill in your email and password to continue"
                )
                Spacer(modifier = Modifier.height(33.dp))
                RediEmailField(
                    value = vm.email,
                    onValueChange = { vm.email = it },
                    label = "Email Address",
                    placeholder = "***********@mail.com"
                )
                RediSecureField(
                    value = vm.password,
                    onValueChange = { vm.password = it },
                    label = "Password",
                    placeholder = "**********"
                )
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        Checkbox(
                            checked = vm.savePassword,
                            onCheckedChange = { vm.savePassword = it })
                        Text(
                            text = "Remember password",
                            color = MaterialTheme.colorScheme.outline,
                            style = MaterialTheme.typography.bodyMedium.copy(fontSize = 12.sp)
                        )
                    }
                    TextButton(onClick = { navController.navigate("ForgotPasScreen") }) {
                        Text(text = "Forgot Password?", fontSize = 12.sp)
                    }
                }

            }
            Column(
                modifier = Modifier
                    .align(Alignment.BottomStart)
                    .fillMaxWidth()
                    .padding(horizontal = 24.dp)
                    .verticalScroll(scrollState)
            ) {
                val authState = LocalAuthState.current
                RediButton(
                    onClick = { vm.signIn() },
                    modifier = Modifier.fillMaxWidth(),
                    enabled = authState.validForm
                ) {
                    Text(text = "Log in", fontSize = 16.sp, fontWeight = FontWeight.W700)
                }
                val annotatedText = buildAnnotatedString {
                    append("Already have an account?")
                    pushStringAnnotation(
                        tag = "URL", annotation = "https://developer.android.com"
                    )
                    withStyle(
                        style = SpanStyle(
                            color = MaterialTheme.colorScheme.primary
                        )
                    ) {
                        append("Sign up")
                    }
                    pop()
                }
                Spacer(modifier = Modifier.height(20.dp))

                ClickableText(
                    text = annotatedText,
                    modifier = Modifier.fillMaxWidth(),
                    onClick = { offset ->

                        annotatedText.getStringAnnotations(
                            tag = "URL", start = offset, end = offset
                        ).firstOrNull()?.let {
                            navController.navigate("SignUpScreen")
                        }
                    },
                    style = MaterialTheme.typography.bodyMedium.copy(
                        textAlign = TextAlign.Center,
                        color = MaterialTheme.colorScheme.outline
                    )
                )
                Spacer(modifier = Modifier.height(20.dp))
                Text(
                    text = "or log in using",
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth(),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.outline

                )
                IconButton(
                    onClick = { /*TODO*/ },
                    modifier = Modifier
                        .fillMaxWidth()
                        .alignBy { it.measuredWidth / 2 }) {
                    Icon(
                        painterResource(id = R.drawable.google),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.secondary
                    )
                }
                Spacer(modifier = Modifier.height(28.dp))
            }
        }

    }
}

@Preview(showBackground = true)
@Composable
fun PreviewSignInScreen() {
    Train2024Theme {
        SignInScreen(navController = rememberNavController())
    }
}