package su.pank.train2024.ui.models

import kotlinx.serialization.Serializable
import java.util.UUID

@Serializable
data class PackageDescription(val items: String, val weight: Float, val price: Float) {
    val trackingNumber = "R-" + UUID.randomUUID()

    fun check() = items.isNotBlank() && weight > 0.0f && price > 0.0f

}
