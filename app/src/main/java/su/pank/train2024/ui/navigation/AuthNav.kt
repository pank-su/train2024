package su.pank.train2024.ui.navigation

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.rizzi.bouquet.ResourceType
import com.rizzi.bouquet.VerticalPDFReader
import com.rizzi.bouquet.rememberVerticalPdfReaderState
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.gotrue.SessionStatus
import io.github.jan.supabase.gotrue.auth
import org.koin.androidx.compose.koinViewModel
import org.koin.compose.koinInject
import su.pank.train2024.R
import su.pank.train2024.ui.screen.auth.forgot.ForgotPasswordScreen
import su.pank.train2024.ui.screen.auth.forgot.ForgotPasswordViewModel
import su.pank.train2024.ui.screen.auth.forgot.OTP
import su.pank.train2024.ui.screen.auth.sign_in.SignInScreen
import su.pank.train2024.ui.screen.auth.sign_up.SignUpScreen

@Composable
fun AuthNav(navController: NavController) {
    val supabaseClient = koinInject<SupabaseClient>()
    val uiState: SessionStatus by supabaseClient.auth.sessionStatus.collectAsState()
    val newController = rememberNavController()
    val currentState by newController.currentBackStackEntryAsState()
    val navControllerCurrentState by navController.currentBackStackEntryAsState()

    LaunchedEffect(uiState, currentState) {
        if (navControllerCurrentState?.destination?.route != "Auth") return@LaunchedEffect
        if (uiState is SessionStatus.Authenticated && ((currentState?.destination?.route) == "SignUpScreen" || (currentState?.destination?.route) == "SignInScreen")) navController.navigate(
            "MainScreen"
        ){
            popUpTo(navController.graph.id){
                inclusive = true
            }
        }
    }

    val vm: ForgotPasswordViewModel = koinViewModel()
    if (uiState == SessionStatus.LoadingFromStorage) CircularProgressIndicator()
    else {
        NavHost(navController = newController, startDestination = "SignUpScreen") {
            composable("SignUpScreen") {
                SignUpScreen(newController)
            }

            composable("SignInScreen") {
                SignInScreen(navController = newController)
            }

            composable("ForgotPasScreen") {
                ForgotPasswordScreen(navController = newController, vm = vm)
            }

            composable("OTP") {
                OTP(navController = newController, vm = vm)
            }

            composable("PDF") {
                val pdfState =
                    rememberVerticalPdfReaderState(resource = ResourceType.Asset(R.raw.test))
                VerticalPDFReader(state = pdfState, modifier = Modifier.fillMaxSize())
            }
        }
    }


    if (uiState == SessionStatus.NetworkError) AlertDialog(onDismissRequest = { }, confirmButton = {
        TextButton(onClick = { }) {
            Text(text = "OK")
        }
    })


}