package su.pank.train2024.ui.screen.main.send

import android.Manifest
import android.annotation.SuppressLint
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text2.input.rememberTextFieldState
import androidx.compose.foundation.text2.input.setTextAndPlaceCursorAtEnd
import androidx.compose.foundation.text2.input.textAsFlow
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import io.eyram.iconsax.IconSax
import io.github.alexzhirkevich.cupertino.Surface
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import org.koin.androidx.compose.koinViewModel
import org.koin.compose.koinInject
import su.pank.train2024.ErrorManager
import su.pank.train2024.R
import su.pank.train2024.data.AddressRepository
import su.pank.train2024.ui.components.RediSimpleField
import su.pank.train2024.ui.models.PackageDescription
import su.pank.train2024.ui.models.PositionDescription
import su.pank.train2024.ui.theme.Train2024Theme


@SuppressLint("MissingPermission")
@OptIn(ExperimentalFoundationApi::class, ExperimentalPermissionsApi::class)
@Composable
fun SendPackagePrepare(navController: NavController, vm: SendPackageViewModel = koinViewModel()) {
    val permission = rememberPermissionState(permission = Manifest.permission.ACCESS_FINE_LOCATION)

    val context = LocalContext.current

    val fusedLocationClient: FusedLocationProviderClient = remember {
        LocationServices.getFusedLocationProviderClient(context)
    }

    val addressRepository: AddressRepository = koinInject()
    val errorManager: ErrorManager = koinInject()


    LazyColumn(modifier = Modifier.padding(start = 24.dp, end = 24.dp, top = 43.dp)) {
        // Origin
        item {
            Column(verticalArrangement = Arrangement.spacedBy(5.dp)) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(6.dp)
                ) {
                    Icon(
                        painter = painterResource(R.drawable.origin),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.primary,
                        modifier = Modifier.size(12.dp)
                    )
                    Text(text = "Origin Details", fontWeight = FontWeight.W500)


                }
                val address = rememberTextFieldState(vm.origin.address)
                val state = rememberTextFieldState(vm.origin.state)
                val phone = rememberTextFieldState(vm.origin.phone)
                val others = rememberTextFieldState(vm.origin.others)

                LaunchedEffect(key1 = permission.status.isGranted) {
                    if (!permission.status.isGranted) permission.launchPermissionRequest()
                    else {
                        fusedLocationClient.lastLocation.addOnSuccessListener {
                            CoroutineScope(Dispatchers.IO).launch {
                                try {
                                    val geoInfo = addressRepository.getAddressByLatLng(
                                        it.latitude, it.longitude
                                    )
                                    address.setTextAndPlaceCursorAtEnd(geoInfo.address.city + ", " + geoInfo.address.road)
                                    state.setTextAndPlaceCursorAtEnd(geoInfo.address.country + ", " + geoInfo.address.state)
                                } catch (e: Exception) {
                                    errorManager.addError(e)
                                }
                            }
                        }
                    }
                }

                RediSimpleField(
                    textFieldState = address, modifier = Modifier.fillMaxWidth(), hint = "Address"
                )
                RediSimpleField(
                    textFieldState = state,
                    modifier = Modifier.fillMaxWidth(),
                    hint = "State,Country"
                )
                RediSimpleField(
                    textFieldState = phone,
                    modifier = Modifier.fillMaxWidth(),
                    hint = "Phone number"
                )
                RediSimpleField(
                    textFieldState = others, modifier = Modifier.fillMaxWidth(), hint = "Others"
                )
                Spacer(Modifier.height(39.dp))
                LaunchedEffect(key1 = null) {
                    combine(
                        address.textAsFlow(),
                        state.textAsFlow(),
                        phone.textAsFlow(),
                        others.textAsFlow()
                    ) { a, s, p, o ->
                        PositionDescription(a.toString(), s.toString(), p.toString(), o.toString())
                    }.collect {
                        vm.origin = it
                    }
                }
            }
        }
        items(vm.dests.size) { index ->
            Column(verticalArrangement = Arrangement.spacedBy(5.dp)) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(6.dp)
                ) {
                    Icon(
                        painter = painterResource(R.drawable.point),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.primary,
                        modifier = Modifier.size(12.dp)
                    )
                    Text(text = "Destination Details", fontWeight = FontWeight.W500)

                }
                val address = rememberTextFieldState(vm.dests[index].address)
                val state = rememberTextFieldState(vm.dests[index].state)
                val phone = rememberTextFieldState(vm.dests[index].phone)
                val others = rememberTextFieldState(vm.dests[index].others)
                RediSimpleField(
                    textFieldState = address, modifier = Modifier.fillMaxWidth(), hint = "Address"
                )
                RediSimpleField(
                    textFieldState = state,
                    modifier = Modifier.fillMaxWidth(),
                    hint = "State,Country"
                )
                RediSimpleField(
                    textFieldState = phone,
                    modifier = Modifier.fillMaxWidth(),
                    hint = "Phone number"
                )
                RediSimpleField(
                    textFieldState = others, modifier = Modifier.fillMaxWidth(), hint = "Others"
                )
                LaunchedEffect(key1 = null) {
                    combine(
                        address.textAsFlow(),
                        state.textAsFlow(),
                        phone.textAsFlow(),
                        others.textAsFlow()
                    ) { a, s, p, o ->
                        PositionDescription(a.toString(), s.toString(), p.toString(), o.toString())
                    }.collect {
                        vm.dests[index] = it
                    }
                }
            }
        }
        item {
            Spacer(Modifier.height(10.dp))

            Row(verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(6.dp),
                modifier = Modifier.clickable {
                    vm.dests.add(PositionDescription("", "", "", ""))
                }) {
                Icon(
                    painter = painterResource(IconSax.Linear.AddSquare),
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.primary,
                    modifier = Modifier.size(14.dp)
                )
                Text(
                    text = "Add destination",
                    style = MaterialTheme.typography.bodySmall,
                    color = MaterialTheme.colorScheme.outline
                )

            }
            Spacer(Modifier.height(10.dp))

        }
        item {
            Column(verticalArrangement = Arrangement.spacedBy(5.dp)) {


                Text(text = "Package Details", fontWeight = FontWeight.W500)

                val items = rememberTextFieldState()
                val weight = rememberTextFieldState()
                val price = rememberTextFieldState()
                RediSimpleField(
                    textFieldState = items,
                    modifier = Modifier.fillMaxWidth(),
                    hint = "package items",
                )
                RediSimpleField(
                    textFieldState = weight,
                    modifier = Modifier.fillMaxWidth(),
                    hint = "Weight of item(kg)",
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal)

                )
                RediSimpleField(
                    textFieldState = price,
                    modifier = Modifier.fillMaxWidth(),
                    hint = "Worth of Items",
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)

                )

                LaunchedEffect(key1 = null) {
                    combine(
                        items.textAsFlow(), weight.textAsFlow(), price.textAsFlow()
                    ) { a, s, p ->
                        PackageDescription(
                            a.toString(),
                            s.toString().toFloatOrNull() ?: 0f,
                            p.toString().toFloatOrNull() ?: 0f
                        )
                    }.collect {
                        vm.packageDescription = it
                    }
                }
            }
            Spacer(Modifier.height(39.dp))

        }
        item {
            Text(text = "Select delivery type", fontWeight = FontWeight.W500)
            Spacer(modifier = Modifier.height(16.dp))
            Row(modifier = Modifier.fillMaxSize()) {
                Card(
                    onClick = {
                        println(vm.checkPoles())
                        if (!vm.checkPoles()) errorManager.addError(Exception("Заполните все поля")) else {
                            navController.navigate("Send a package ")
                        }
                    }, modifier = Modifier.weight(1f), colors = CardDefaults.cardColors(
                        containerColor = MaterialTheme.colorScheme.primary,
                        contentColor = MaterialTheme.colorScheme.onPrimary
                    )
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 12.dp)
                    ) {
                        Icon(
                            painter = painterResource(id = IconSax.Linear.Clock1),
                            contentDescription = null
                        )
                        Text(
                            text = "Instant delivery",
                            textAlign = TextAlign.Center,
                            fontSize = 14.sp,
                            fontWeight = FontWeight.W500
                        )
                    }
                }
                Spacer(modifier = Modifier.width(24.dp))
                Card(
                    onClick = { /*TODO*/ },
                    modifier = Modifier
                        .shadow(2.dp)
                        .weight(1f),
                    colors = CardDefaults.cardColors(
                        containerColor = MaterialTheme.colorScheme.surface,
                        contentColor = MaterialTheme.colorScheme.outline
                    )
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 12.dp)
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.calender),
                            contentDescription = null
                        )
                        Text(
                            text = "Scheduled delivery",
                            textAlign = TextAlign.Center,
                            fontSize = 14.sp,
                            fontWeight = FontWeight.W500
                        )
                    }
                }
            }
        }
    }
}

@Preview
@Composable
private fun PreviewSendPackagePrepare() {
    Train2024Theme {
        Surface {
            SendPackagePrepare(rememberNavController())
        }
    }
}