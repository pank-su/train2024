package su.pank.train2024.ui.screen.auth.forgot

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.gotrue.OtpType
import io.github.jan.supabase.gotrue.auth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import su.pank.train2024.ErrorManager
import kotlin.concurrent.timer
import kotlin.time.Duration.Companion.seconds

class ForgotPasswordViewModel(
    private val client: SupabaseClient,
    private val errorManager: ErrorManager
) : ViewModel() {
    var email by mutableStateOf("")
    var otp by mutableStateOf("")
    var timer = timer(null, daemon = true, initialDelay = 1000L, period = 1000L) {
        if (_timeToEnd.value.isPositive()) {
            _timeToEnd.value -= 1.seconds
        }
    }
    private val _timeToEnd = MutableStateFlow(0.seconds)
    val timeToEnd = _timeToEnd.asStateFlow()


    fun sendOTP(navController: NavController) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                client.auth.resetPasswordForEmail(email)
                withContext(Dispatchers.Main) {
                    navController.navigate("OTP")
                    _timeToEnd.value = 70.seconds
                }
            } catch (e: Exception) {

                errorManager.addError(e)
            }
        }
    }

    fun verifyOTP(navController: NavController) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                client.auth.verifyEmailOtp(OtpType.Email.RECOVERY, email = email, token = otp)
            } catch (e: Exception) {
                errorManager.addError(e)
            }
        }
    }


}