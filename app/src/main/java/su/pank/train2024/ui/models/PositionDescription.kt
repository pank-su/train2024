package su.pank.train2024.ui.models

import kotlinx.serialization.Serializable

@Serializable
data class PositionDescription(
    val address: String,
    val state: String,
    val phone: String,
    val others: String
){
    fun check(): Boolean = address.isNotBlank() && state.isNotBlank() && phone.isNotBlank()

}