package su.pank.train2024.ui.screen.main.home

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.navigation.NavController

@Composable
fun HomeScreen(navController: NavController) {
    Column {
        Button(onClick = { navController.navigate("Add Payment method") }) {
            Text(text = "Add Payment method")
            
        }
        Button(onClick = { navController.navigate("Send a package") }) {
            Text(text = "Send a package")

        }
    }
}