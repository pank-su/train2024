package su.pank.train2024.ui.components

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import su.pank.train2024.ui.theme.Train2024Theme

@Composable
fun RediOTP(value: String, onSetValue: (String) -> Unit) {
    BasicTextField(
        value = value,
        onValueChange = onSetValue,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        decorationBox = {
            Row(horizontalArrangement = Arrangement.spacedBy(30.dp)) {
                repeat(6) {
                    Box(
                        modifier = Modifier
                            .size(32.dp)
                            .border(1.dp, MaterialTheme.colorScheme.outline)
                    ) {
                        Text(
                            text = (value.getOrNull(it) ?: "").toString(),
                            modifier = Modifier.align(
                                Alignment.Center
                            )
                        )
                    }
                }
            }
        })
}

@Preview
@Composable
private fun PreviewRediOTP() {
    var value by remember {
        mutableStateOf("")
    }

    Train2024Theme {
        Surface {
            RediOTP(value = value) {
                value = it
            }
        }
    }
}