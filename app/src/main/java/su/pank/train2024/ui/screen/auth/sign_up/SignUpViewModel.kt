package su.pank.train2024.ui.screen.auth.sign_up

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.gotrue.providers.builtin.Email
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import su.pank.train2024.ErrorManager

class SignUpViewModel(
    private val supabaseClient: SupabaseClient,
    private val errorManager: ErrorManager
) : ViewModel() {
    fun signUp() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                supabaseClient.auth.signUpWith(Email) {
                    email = this@SignUpViewModel.email
                    password = this@SignUpViewModel.password
                }
            } catch (e: Exception) {
                errorManager.addError(e)
            }
        }
    }

    var name: String by mutableStateOf("")
    var phone: String by mutableStateOf("")
    var email: String by mutableStateOf("")

    var password: String by mutableStateOf("")
    var password2: String by mutableStateOf("")

    var isAgree: Boolean by mutableStateOf(false)

}