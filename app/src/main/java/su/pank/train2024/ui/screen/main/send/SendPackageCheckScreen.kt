package su.pank.train2024.ui.screen.main.send

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import org.koin.androidx.compose.koinViewModel
import su.pank.train2024.ui.components.RediButton
import su.pank.train2024.ui.components.RediOutlinedButton

@Composable
fun SendPackageCheckScreen(navController: NavController, viewModel: SendPackageViewModel = koinViewModel()) {
    val scrollState = rememberScrollState()
    Column(
        Modifier
            .fillMaxWidth()
            .padding(24.dp)
            .verticalScroll(scrollState)
    ) {
        Text(
            text = "Package Information",
            style = MaterialTheme.typography.bodyLarge,
            fontSize = 16.sp,
            fontWeight = FontWeight.W500,
            color = MaterialTheme.colorScheme.primary
        )
        Text(text = "Origin details")

        Text(
            text = "${viewModel.origin.address}, ${viewModel.origin.state}",
            color = MaterialTheme.colorScheme.outline
        )
        Text(text = viewModel.origin.phone, color = MaterialTheme.colorScheme.outline)


        Text(text = "Destination details")

        viewModel.dests.forEachIndexed { index, positionDescription ->
            Text(
                text = "${index + 1} ${positionDescription.address}, ${positionDescription.state}",
                color = MaterialTheme.colorScheme.outline
            )
            Text(text = positionDescription.phone, color = MaterialTheme.colorScheme.outline)
        }

        Text(text = "Other details")
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(text = "Package Items", color = MaterialTheme.colorScheme.outline)
            Text(
                text = viewModel.packageDescription.items,
                color = MaterialTheme.colorScheme.secondary
            )
        }
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(text = "Weight of items", color = MaterialTheme.colorScheme.outline)
            Text(
                text = viewModel.packageDescription.weight.toString(),
                color = MaterialTheme.colorScheme.secondary
            )
        }
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(text = "Worth of Items", color = MaterialTheme.colorScheme.outline)
            Text(
                text = viewModel.packageDescription.price.toString(),
                color = MaterialTheme.colorScheme.secondary
            )
        }
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(text = "Tracking Number", color = MaterialTheme.colorScheme.outline)
            Text(
                text = viewModel.packageDescription.trackingNumber,
                color = MaterialTheme.colorScheme.secondary
            )
        }
        Spacer(Modifier.height(37.dp))

        HorizontalDivider()

        Text(
            text = "Charges",
            fontWeight = FontWeight.W500,
            style = MaterialTheme.typography.bodyLarge,
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.primary
        )

        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(text = "Delivery Charges", color = MaterialTheme.colorScheme.outline)
            Text(
                text = viewModel.deliveryCharges.toFloat().toString(),
                color = MaterialTheme.colorScheme.secondary
            )
        }
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(text = "Instant delivery", color = MaterialTheme.colorScheme.outline)
            Text(
                text = viewModel.instantDelivery.toFloat().toString(),
                color = MaterialTheme.colorScheme.secondary
            )
        }
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(text = "Tracking Number", color = MaterialTheme.colorScheme.outline)
            Text(text = viewModel.tax.toString(), color = MaterialTheme.colorScheme.secondary)
        }
        HorizontalDivider()
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(text = "Package total", color = MaterialTheme.colorScheme.outline)
            Text(text = viewModel.sum.toString(), color = MaterialTheme.colorScheme.secondary)
        }
        Spacer(Modifier.height(46.dp))

        Row(horizontalArrangement = Arrangement.spacedBy(24.dp)) {
            RediOutlinedButton(onClick = {  }, Modifier.weight(1f)) {
                Text(text = "Edit package", fontSize = 16.sp, fontWeight = FontWeight.W700)

            }
            RediButton(onClick = { viewModel.makePayment(navController) }, Modifier.weight(1f)) {
                Text(text = "Make payment", fontSize = 16.sp, fontWeight = FontWeight.W700)

            }
        }


    }
}
