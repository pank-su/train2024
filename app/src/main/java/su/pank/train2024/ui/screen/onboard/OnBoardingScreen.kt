package su.pank.train2024.ui.screen.onboard

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import org.koin.androidx.compose.koinViewModel
import org.koin.core.parameter.parametersOf
import su.pank.train2024.R
import su.pank.train2024.ui.components.RediButton
import su.pank.train2024.ui.components.RediOutlinedButton
import su.pank.train2024.ui.models.OnBoardInfo
import su.pank.train2024.ui.theme.Train2024Theme
import java.util.LinkedList
import java.util.Queue

// Генерация базовой последовательсности
fun generateDefaultQueue(): Queue<OnBoardInfo> {
    val linkedList = LinkedList<OnBoardInfo>()
    linkedList.addAll(
        listOf(
            OnBoardInfo(
                R.drawable.first,
                "Quick Delivery At Your Doorstep",
                "Enjoy quick pick-up and delivery to your destination"
            ),
            OnBoardInfo(
                R.drawable.second,
                "Flexible Payment",
                "Different modes of payment either before and after delivery without stress"
            ),
            OnBoardInfo(
                R.drawable.third,
                "Real-time Tracking",
                "Track your packages/items from the comfort of your home till final destination"
            )
        )
    )
    return linkedList as Queue<OnBoardInfo>
}

@Composable
fun OnBoardingScreen(
    navController: NavController,
    queue: Queue<OnBoardInfo> = generateDefaultQueue()
) {
    val viewModel: OnBoardingScreenViewModel =
        koinViewModel(parameters = { parametersOf(queue, navController) })

    val info by viewModel.info.collectAsState()
    val isLast by viewModel.isLast.collectAsState(initial = false)

    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState),
        verticalArrangement = Arrangement.Center
    ) {
        AnimatedContent(targetState = info, label = "Animate") {


            Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                Column(
                    modifier = Modifier.fillMaxWidth(0.9f),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(
                        painter = painterResource(id = it.image),
                        contentDescription = null,
                        modifier = Modifier.width(346.dp).semantics { contentDescription = "Image" },
                        contentScale = ContentScale.FillWidth
                    )
                    Spacer(modifier = Modifier.height(48.dp))

                    Text(
                        text = it.title,
                        textAlign = TextAlign.Center,
                        style = MaterialTheme.typography.headlineSmall,
                        color = MaterialTheme.colorScheme.primary,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier.fillMaxWidth(0.9f).semantics { contentDescription = "Title" }
                    )
                    Text(
                        text = it.description,
                        textAlign = TextAlign.Center,
                        color = Color(0xFF3A3A3A),
                        modifier = Modifier.fillMaxWidth(0.8f).semantics { contentDescription = "Description" }

                    )
                }
            }
        }
        Spacer(modifier = Modifier.height(82.dp))

        AnimatedVisibility(visible = !isLast) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 24.dp)
            ) {
                RediOutlinedButton(
                    onClick = { viewModel.skip() },
                    modifier = Modifier.width(100.dp).semantics { contentDescription = "Skip" }
                ) {
                    Text(text = "Skip")
                }
                RediButton(
                    onClick = { viewModel.nextScreen() },
                    modifier = Modifier.width(100.dp).semantics { contentDescription = "Next_btn" }
                ) {
                    Text(text = "Next")
                }
            }

        }
        AnimatedVisibility(visible = isLast) {
            Column {
                RediButton(
                    onClick = { viewModel.skip() },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 24.dp).semantics { contentDescription = "SignUp_btn" }
                ) {
                    Text(text = "Sign Up")
                }
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(text = "Already have an account?", color = Color(0xFFA7A7A7))
                    Text(text = "Sign in", color = MaterialTheme.colorScheme.primary)
                }
            }
        }

    }

}

@Preview
@Composable
fun PreviewOnBoardingScreen() {
    Train2024Theme {
        Surface {
            OnBoardingScreen(rememberNavController())
        }
    }
}

@Preview(device = "spec:parent=pixel_7_pro,orientation=landscape")
@Composable
fun PreviewRotateOnBoardingScreen() {
    Train2024Theme {
        Surface {
            OnBoardingScreen(rememberNavController())
        }
    }
}