package su.pank.train2024.ui.screen.auth.sign_in

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.gotrue.providers.builtin.Email
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import su.pank.train2024.ErrorManager

class SignInViewModel(
    private val supabaseClient: SupabaseClient,
    private val errorManager: ErrorManager
) : ViewModel() {

    var email: String by mutableStateOf("")

    var password: String by mutableStateOf("")

    var savePassword: Boolean by mutableStateOf(false)


    fun signIn() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                supabaseClient.auth.signInWith(Email) {
                    email = this@SignInViewModel.email
                    password = this@SignInViewModel.password
                }
            } catch (e: Exception) {
                errorManager.addError(e)
            }

        }

    }
}