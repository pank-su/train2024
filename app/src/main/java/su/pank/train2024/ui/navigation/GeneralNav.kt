package su.pank.train2024.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import su.pank.train2024.ui.screen.MainScreen
import su.pank.train2024.ui.screen.SplashScreen
import su.pank.train2024.ui.screen.onboard.OnBoardingScreen


@Composable
fun GeneralNav() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = "SplashScreen") {
        composable("SplashScreen") {
            SplashScreen(navController)
        }

        composable("OnBoardScreen") {
            OnBoardingScreen(navController = navController)
        }
        composable("Holder") {
            LaunchedEffect(null) {
                navController.navigate("Auth") {
                    popUpTo(0) {
                        inclusive = true
                    }
                }
            }
        }
        composable("Auth"){
            AuthNav(navController = navController)
        }

        composable("MainScreen"){
            MainScreen(navController)
        }


    }
}