package su.pank.train2024.ui.screen

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import io.eyram.iconsax.IconSax
import io.github.alexzhirkevich.cupertino.CupertinoIcon
import io.github.alexzhirkevich.cupertino.CupertinoIconButton
import io.github.alexzhirkevich.cupertino.CupertinoNavigationBar
import io.github.alexzhirkevich.cupertino.CupertinoNavigationBarItem
import io.github.alexzhirkevich.cupertino.CupertinoText
import io.github.alexzhirkevich.cupertino.CupertinoTopAppBar
import io.github.alexzhirkevich.cupertino.ExperimentalCupertinoApi
import io.github.alexzhirkevich.cupertino.Surface
import su.pank.train2024.R
import su.pank.train2024.ui.models.MainDestination
import su.pank.train2024.ui.navigation.MainNav
import su.pank.train2024.ui.screen.main.home.HomeScreen
import su.pank.train2024.ui.screen.main.profile.ProfileScreen
import su.pank.train2024.ui.theme.Train2024Theme

@OptIn(ExperimentalCupertinoApi::class)
@Composable
fun MainScreen(parentNavController: NavController) {
    val dests =
        listOf(MainDestination(
            "Home",
            {
                CupertinoIcon(
                    painterResource(id = if (it) IconSax.Outline.Home2 else IconSax.Bold.Home2),
                    null
                )
            }) { HomeScreen(navController = it) },
            MainDestination(
                "Wallet",
                {
                    CupertinoIcon(
                        painterResource(id = if (it) IconSax.Outline.Wallet3 else IconSax.Bold.Wallet3),

                        null
                    )
                }

            ) {},
            MainDestination("Track", {
                CupertinoIcon(
                    painterResource(id = if (it) IconSax.Outline.SmartCar else IconSax.Bold.SmartCar),

                    null
                )
            }) {},
            MainDestination("Profile", {
                if (it)
                    CupertinoIcon(
                        painterResource(id = IconSax.Outline.ProfileCircle),

                        null
                    ) else {
                    Image(
                        painter = painterResource(id = R.drawable.profile_circle),
                        contentDescription = null
                    )
                }
            }) {
                ProfileScreen(navController = it)
            })

    val navController = rememberNavController()
    val currentScreen by navController.currentBackStackEntryAsState()

    val title by remember(currentScreen) {
        derivedStateOf {
            currentScreen?.destination?.route
        }
    }

    val isMainScreen by remember(title) {
        derivedStateOf { dests.any { it.name == title } || title == "Transaction" }
    }
    LaunchedEffect(key1 = isMainScreen) {
        println(isMainScreen)
    }


    Scaffold(
        modifier = Modifier.fillMaxSize(),
        containerColor = MaterialTheme.colorScheme.surface,
        topBar = {
            AnimatedVisibility(!isMainScreen) {
                CupertinoTopAppBar(navigationIcon = {
                    CupertinoIconButton(onClick = { navController.popBackStack() }) {
                        CupertinoIcon(
                            painterResource(id = IconSax.Linear.ArrowSquareLeft),
                            contentDescription = null,
                            modifier = Modifier.padding(start = 15.dp)
                        )
                    }

                }, title = {
                    CupertinoText(
                        text = title.toString(), color = MaterialTheme.colorScheme.outline
                    )
                }, modifier = Modifier.shadow(5.dp))
            }
        },
        bottomBar = {
            AnimatedVisibility(isMainScreen && (title != "Transaction") ) {
                CupertinoNavigationBar(isTranslucent = false, divider = {}) {
                    val navBackStackEntry by navController.currentBackStackEntryAsState()
                    val currentDestination = navBackStackEntry?.destination
                    dests.forEach { dest ->
                        val isSelected =
                            currentDestination?.hierarchy?.any { it.route == dest.name } == true

                        CupertinoNavigationBarItem(
                            selected = isSelected,
                            onClick = {
                                navController.navigate(dest.name) {
                                    // Pop up to the start destination of the graph to
                                    // avoid building up a large stack of destinations
                                    // on the back stack as users select items
                                    popUpTo(navController.graph.findStartDestination().id) {
                                        saveState = true
                                    }
                                    // Avoid multiple copies of the same destination when
                                    // reselecting the same item
                                    launchSingleTop = true
                                    // Restore state when reselecting a previously selected item
                                    restoreState = true
                                }
                            },
                            label = { CupertinoText(text = dest.name) },
                            icon = {
                                dest.icon(currentDestination?.hierarchy?.any { it.route == dest.name } != true)
                            }
                        )
                    }
                }
            }
        }) {
        MainNav(
            navController = navController,
            parentNavController,
            modifier = Modifier.padding(it),
            dests = dests
        )
    }
}

@Preview
@Composable
private fun PreviewMainScreen() {
    Train2024Theme {
        Surface {
            MainScreen(rememberNavController())
        }
    }
}