package su.pank.train2024.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.delay
import su.pank.train2024.R
import su.pank.train2024.ui.theme.Train2024Theme

@Composable
fun SplashScreen(navController: NavController) {
    LaunchedEffect(null){
        delay(1000)
        navController.navigate("OnBoardScreen")
    }
    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Image(
            painter = painterResource(id = R.drawable.splash),
            contentDescription = null,
            modifier = Modifier
                .align(
                    Alignment.Center
                )
                .fillMaxWidth(0.7f),
            contentScale = ContentScale.FillWidth
        )
    }
}

@Preview
@Composable
fun PreviewSplashScreen() {
    Train2024Theme {
        Surface {
            SplashScreen(rememberNavController())
        }
    }
}