package su.pank.train2024.ui.screen.auth.forgot

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import io.github.jan.supabase.annotations.SupabaseExperimental
import io.github.jan.supabase.compose.auth.ui.AuthForm
import org.koin.androidx.compose.koinViewModel
import su.pank.train2024.ui.components.RediButton
import su.pank.train2024.ui.components.RediOTP
import su.pank.train2024.ui.components.RediTitle
import kotlin.time.Duration

@OptIn(SupabaseExperimental::class)
@Composable
fun OTP(
    modifier: Modifier = Modifier,
    navController: NavController,
    vm: ForgotPasswordViewModel = koinViewModel()
) {
    val timeToEnd by vm.timeToEnd.collectAsState()
    AuthForm {
        Column(
            modifier = Modifier
                .padding(start = 24.dp, end = 24.dp)

        ) {
            Spacer(modifier = Modifier.height(110.dp))

            RediTitle(
                title = "Forgot Password",
                description = "Enter your email address"
            )
            Spacer(modifier = Modifier.height(56.dp))
            RediOTP(value = vm.otp) {
                vm.otp = it.take(6)
            }
            if (timeToEnd > Duration.ZERO) {
                Text(
                    text = "If you didn’t receive code, resend after ${timeToEnd.inWholeMinutes}:${
                        "%02d".format(
                            timeToEnd.inWholeSeconds - timeToEnd.inWholeMinutes * 60
                        )
                    }"
                )
            } else {
                val annotatedString = buildAnnotatedString {
                    append("If you didn’t receive code, ")
                    pushStringAnnotation("BTN", "")
                    withStyle(SpanStyle(MaterialTheme.colorScheme.primary)) {
                        append("resend")
                    }
                    pop()

                }
                ClickableText(
                    text = annotatedString,
                    style = MaterialTheme.typography.bodyMedium.copy(MaterialTheme.colorScheme.outline)
                ) {
                    annotatedString.getStringAnnotations("BTN", it, it).firstOrNull()?.let {
                        vm.sendOTP(navController)
                    }
                }
            }
            Spacer(modifier = Modifier.height(82.dp))

            RediButton(
                onClick = { vm.verifyOTP(navController) },
                modifier = Modifier.fillMaxWidth(),
                enabled = vm.otp.length == 6
            ) {
                Text(text = "Set New Password", fontSize = 16.sp, fontWeight = FontWeight.W700)
            }
        }
    }
}