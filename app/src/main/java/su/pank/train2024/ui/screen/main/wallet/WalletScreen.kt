package su.pank.train2024.ui.screen.main.wallet

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.github.alexzhirkevich.cupertino.CupertinoScaffold
import io.github.alexzhirkevich.cupertino.CupertinoText
import io.github.alexzhirkevich.cupertino.CupertinoTopAppBar
import io.github.alexzhirkevich.cupertino.ExperimentalCupertinoApi
import io.github.alexzhirkevich.cupertino.Surface
import su.pank.train2024.ui.theme.Train2024Theme

@OptIn(ExperimentalCupertinoApi::class)
@Composable
fun WalletScreen() {
    //val vm: ProfileViewModel = koinViewModel()

    CupertinoScaffold(topBar = {
        CupertinoTopAppBar(title = {
            CupertinoText(
                text = "Wallet",
                color = MaterialTheme.colorScheme.outline
            )
        }, isTranslucent = false, modifier = Modifier.shadow(3.dp))
    }) {
        LazyColumn(modifier = Modifier.padding(it).padding(horizontal = 24.dp)) {
            item {
                //RediProfileBallance(imageName = vm.image, userName = vm.name, userBallance = 1000f)
            }
            item { 
                Spacer(modifier = Modifier.height(28.dp))
                Card (shape = RoundedCornerShape(8.dp), colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.onSurfaceVariant)){
                    Column(modifier = Modifier.fillMaxWidth()) {
                        Text(text = "Top up", style = MaterialTheme.typography.bodyLarge, fontWeight = FontWeight.W500)
                        Row {

                        }
                    }
                }
                Spacer(modifier = Modifier.height(41.dp))
            }
        }
    }
}

@Preview
@Composable
private fun PreviewWallet() {
    Train2024Theme {
        Surface {
            WalletScreen()
        }
    }
}