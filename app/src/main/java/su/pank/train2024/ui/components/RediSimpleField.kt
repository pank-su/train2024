package su.pank.train2024.ui.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text2.BasicTextField2
import androidx.compose.foundation.text2.input.InputTransformation
import androidx.compose.foundation.text2.input.TextFieldState
import androidx.compose.foundation.text2.input.rememberTextFieldState
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import su.pank.train2024.ui.theme.Train2024Theme

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun RediSimpleField(
    textFieldState: TextFieldState,
    modifier: Modifier = Modifier,
    hint: String = "",
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    inputTransformation: InputTransformation? = null
) {
    BasicTextField2(
        state = textFieldState,
        keyboardOptions = keyboardOptions,
        inputTransformation = inputTransformation,
        modifier = modifier
            .height(32.dp)
            .shadow(2.dp),
        decorator = { innerTextField ->
            Box(
                Modifier
                    .fillMaxSize()
                    .background(MaterialTheme.colorScheme.surface)
                    .padding(6.dp, 7.dp, 0.dp, 9.dp),
            ) {
                if (textFieldState.text.isEmpty()) Text(
                    text = hint,
                    style = MaterialTheme.typography.bodySmall,
                    color = MaterialTheme.colorScheme.onSurfaceVariant,
                    modifier = Modifier.align(Alignment.CenterStart)
                )
                innerTextField()

            }
        })
}


@OptIn(ExperimentalFoundationApi::class)
@Preview
@Composable
private fun PreviewRediSimpleField() {
    Train2024Theme {
        val state = rememberTextFieldState()
        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxHeight()
        ) {
            RediSimpleField(
                state,
                hint = "Ok",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp)
            )
            RediSimpleField(
                state,
                hint = "Ok",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp)
            )
            RediSimpleField(
                state,
                hint = "Ok",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp)
            )
            RediSimpleField(
                state,
                hint = "Ok",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp)
            )
            RediSimpleField(
                state,
                hint = "Ok",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp)
            )
            RediSimpleField(
                state,
                hint = "Ok",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp)
            )

        }
    }
}