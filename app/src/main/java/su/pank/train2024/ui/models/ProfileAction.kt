package su.pank.train2024.ui.models

import androidx.annotation.DrawableRes
import androidx.compose.ui.graphics.Color

data class ProfileAction(
    val title: String,
    val description: String,
    @DrawableRes val icon: Int,
    val color: Color? = null,
    val onClick: () -> Unit,
)
