package su.pank.train2024.ui.screen.auth.sign_up

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import io.github.jan.supabase.annotations.SupabaseExperimental
import io.github.jan.supabase.compose.auth.ui.AuthForm
import io.github.jan.supabase.compose.auth.ui.LocalAuthState
import io.github.jan.supabase.compose.auth.ui.password.PasswordRule
import org.koin.androidx.compose.koinViewModel
import su.pank.train2024.R
import su.pank.train2024.ui.components.RediButton
import su.pank.train2024.ui.components.RediEmailField
import su.pank.train2024.ui.components.RediField
import su.pank.train2024.ui.components.RediPhoneField
import su.pank.train2024.ui.components.RediSecureField
import su.pank.train2024.ui.components.RediTitle
import su.pank.train2024.ui.theme.Train2024Theme


@OptIn(SupabaseExperimental::class)
@Composable
fun SignUpScreen(navController: NavHostController) {
    val scrollState = rememberScrollState()
    val vm: SignUpViewModel = koinViewModel()

    Column(
        modifier = Modifier
            .padding(start = 24.dp, end = 24.dp)
            .verticalScroll(scrollState)
    ) {
        Spacer(modifier = Modifier.height(33.dp))
        AuthForm {
            RediTitle(
                title = "Create an account",
                description = "Complete the sign up process to get started"
            )
            Spacer(modifier = Modifier.height(33.dp))
            RediField(
                value = vm.name,
                onValueChange = { vm.name = it },
                label = "Full name",
                placeholder = "Ivanov Ivan"
            )
            RediPhoneField(
                value = vm.phone,
                onValueChange = { vm.phone = it },
                label = "Phone Number",
                placeholder = "+7(999)999-99-99"
            )
            RediEmailField(
                value = vm.email,
                onValueChange = { vm.email = it },
                label = "Email Address",
                placeholder = "***********@mail.com"
            )
            RediSecureField(
                value = vm.password,
                onValueChange = { vm.password = it },
                label = "Password",
                placeholder = "**********",
                rules = listOf(PasswordRule("Пароли должны совпадать") { it == vm.password2 }, PasswordRule.minLength(4))

            )
            RediSecureField(
                value = vm.password2,
                onValueChange = { vm.password2 = it },
                label = "Confirm Password",
                placeholder = "**********",
                rules = listOf(PasswordRule("Пароли должны совпадать") { it == vm.password })
            )
            Spacer(modifier = Modifier.height(13.dp))
            Row {
                Checkbox(checked = vm.isAgree, onCheckedChange = {
                    vm.isAgree = it
                })
                val annotatedText = buildAnnotatedString {
                    append("By ticking this box, you agree to our ")
                    pushStringAnnotation(
                        tag = "URL", annotation = "https://developer.android.com"
                    )
                    withStyle(
                        style = SpanStyle(
                            color = Color(0xFFFFC107)
                        )
                    ) {
                        append("Terms and conditions and private policy")
                    }
                    pop()
                }
                val context = LocalContext.current
                ClickableText(
                    text = annotatedText,
                    onClick = { offset ->

                        annotatedText.getStringAnnotations(
                            tag = "URL", start = offset, end = offset
                        ).firstOrNull()?.let { annotation ->
                            navController.navigate("PDF")

                        }
                    },
                    style = MaterialTheme.typography.bodySmall.copy(
                        textAlign = TextAlign.Center,
                        color = MaterialTheme.colorScheme.outline
                    )
                )
            }

            Spacer(modifier = Modifier.height(64.dp))
            val authState = LocalAuthState.current
            RediButton(
                onClick = { vm.signUp() },
                modifier = Modifier.fillMaxWidth(),
                enabled = authState.validForm
            ) {
                Text(text = "Sign Up", fontSize = 16.sp, fontWeight = FontWeight.W700)
            }
            val annotatedText = buildAnnotatedString {
                append("Already have an account?")
                pushStringAnnotation(
                    tag = "URL", annotation = "https://developer.android.com"
                )
                withStyle(
                    style = SpanStyle(
                        color = MaterialTheme.colorScheme.primary
                    )
                ) {
                    append("Sign in")
                }
                pop()
            }
            Spacer(modifier = Modifier.height(20.dp))

            ClickableText(
                text = annotatedText,
                modifier = Modifier.fillMaxWidth(),
                onClick = { offset ->

                    annotatedText.getStringAnnotations(
                        tag = "URL", start = offset, end = offset
                    ).firstOrNull()?.let {
                        navController.navigate("SignInScreen")
                    }
                },
                style = MaterialTheme.typography.bodyMedium.copy(
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colorScheme.outline
                )
            )
            Spacer(modifier = Modifier.height(20.dp))
            Text(
                text = "or sign in using",
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth(),
                style = MaterialTheme.typography.bodyMedium,
                color = MaterialTheme.colorScheme.outline

            )
            IconButton(onClick = { /*TODO*/ }, modifier =  Modifier.fillMaxWidth().alignBy { it.measuredWidth / 2 } ) {
                Icon(
                    painterResource(id = R.drawable.google),
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.secondary
                )
            }
            Spacer(modifier = Modifier.height(28.dp))
        }

    }
}

@Preview(showBackground = true)
@Composable
fun PreviewSignUpScreen() {
    Train2024Theme {
        SignUpScreen(navController = rememberNavController())
    }
}