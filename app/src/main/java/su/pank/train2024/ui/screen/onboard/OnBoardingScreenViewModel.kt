package su.pank.train2024.ui.screen.onboard

import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import su.pank.train2024.data.onboardingseen.OnBoardingSeenRepository
import su.pank.train2024.ui.models.OnBoardInfo
import java.util.Queue

class OnBoardingScreenViewModel(
    private val queue: Queue<OnBoardInfo>,
    private val navController: NavController,
    private val repository: OnBoardingSeenRepository
) : ViewModel() {
    private val _info = MutableStateFlow(queue.remove())
    val info = _info.asStateFlow()
    val isLast = info.transform { emit(queue.isEmpty()) }
    var isSkiped = false

    init {
        CoroutineScope(Dispatchers.Main).launch {
            repository.isViewed.collect {
                if (it) skip()
            }
        }
    }

    fun nextScreen() {
        if (queue.isEmpty()) {
            skip()
            return
        }
        _info.value = queue.remove()
    }

    fun skip() {
        if (isSkiped) return
        isSkiped = true
        CoroutineScope(Dispatchers.Main).launch { repository.toggleViewing() }
        navController.navigate("Holder")

    }
}