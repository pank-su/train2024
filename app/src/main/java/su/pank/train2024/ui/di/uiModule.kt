package su.pank.train2024.ui.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import su.pank.train2024.ui.screen.auth.forgot.ForgotPasswordViewModel
import su.pank.train2024.ui.screen.auth.sign_in.SignInViewModel
import su.pank.train2024.ui.screen.auth.sign_up.SignUpViewModel
import su.pank.train2024.ui.screen.main.profile.ProfileViewModel
import su.pank.train2024.ui.screen.main.send.SendPackageViewModel
import su.pank.train2024.ui.screen.onboard.OnBoardingScreenViewModel

val uiModule = module {
    viewModel {
        OnBoardingScreenViewModel(it[0], it[1], get())
    }
    viewModel {
        SignUpViewModel(get(), get())
    }
    viewModel {
        SignInViewModel(get(), get())
    }
    viewModel {
        ForgotPasswordViewModel(get(), get())
    }
    viewModel {
        ProfileViewModel(get(), get())
    }
    viewModel {
        SendPackageViewModel(get(), get())
    }
}