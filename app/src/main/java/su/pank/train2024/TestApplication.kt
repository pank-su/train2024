package su.pank.train2024

import android.app.Application
import android.content.Context
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import su.pank.train2024.data.di.fakedataModule
import su.pank.train2024.ui.di.uiModule

class TestApplication : Application() {
    companion object {
        lateinit var appContext: Context
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        startKoin {
            androidContext(applicationContext)
            modules(fakedataModule, uiModule, module {
                single { ErrorManager() }
            })

        }
    }
}