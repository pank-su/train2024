package su.pank.train2024

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class ErrorManager {
    private val _error = MutableStateFlow<String?>(null)
    val error = _error.asStateFlow()
    fun addError(error: Exception){

        _error.value = error.message
    }

    fun skipError(){
        _error.value = null
    }
}