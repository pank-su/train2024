package su.pank.train2024

import android.app.Application
import android.content.Context
import coil.ImageLoader
import coil.ImageLoaderFactory
import io.github.jan.supabase.annotations.SupabaseExperimental
import io.github.jan.supabase.coil.coil
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import su.pank.train2024.data.SupabaseRepository
import su.pank.train2024.data.di.dataModule
import su.pank.train2024.domain.di.domainModule
import su.pank.train2024.ui.di.uiModule

class MainApplication : Application(), ImageLoaderFactory {
    companion object {
        lateinit var appContext: Context
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        startKoin {
            androidContext(applicationContext)
            modules(dataModule, uiModule, module {
                single { ErrorManager() }
            }, domainModule)

        }
    }

    @OptIn(SupabaseExperimental::class)
    override fun newImageLoader(): ImageLoader = ImageLoader.Builder(this).components {
        add(SupabaseRepository.client.coil)
    }.build()
}