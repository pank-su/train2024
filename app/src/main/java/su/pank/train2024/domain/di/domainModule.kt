package su.pank.train2024.domain.di

import org.koin.dsl.module
import su.pank.train2024.domain.ProfileUseCase

val domainModule = module {
    single {
        ProfileUseCase(get())
    }
}