package su.pank.train2024.domain

import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.postgrest.from
import su.pank.train2024.data.model.ProfileDTO

class ProfileUseCase(private val client: SupabaseClient) {
    suspend operator fun invoke(): ProfileDTO? {
        return client.from("profile").select() {
            filter {
                ProfileDTO::id eq client.auth.currentUserOrNull()?.id
            }
        }.decodeSingleOrNull()
    }
}