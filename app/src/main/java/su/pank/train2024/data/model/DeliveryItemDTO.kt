package su.pank.train2024.data.model

import kotlinx.serialization.Serializable

@Serializable
data class DeliveryItemDTO(val id: Int? = null, val profile_id: String, val package_details: PackageDetailsDTO, val origin: LatLng)

