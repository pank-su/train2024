package su.pank.train2024.data

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.get
import io.ktor.http.appendPathSegments
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import su.pank.train2024.data.model.GEODTO

class AddressRepository {

    private val url = "https://nominatim.openstreetmap.org/"

    private val client = HttpClient(OkHttp) {
        install(ContentNegotiation){
            json(Json{
                ignoreUnknownKeys = true
            })
        }
        install(Logging)
    }

    suspend fun getAddressByLatLng(lat: Double, lon: Double): GEODTO =
        client.get(url) {
            url {
                appendPathSegments("reverse")
                parameters.append("lat", lat.toString())
                parameters.append("lon", lon.toString())
                parameters.append("format", "json")
            }
        }.body()

}