package su.pank.train2024.data.di

import org.koin.dsl.module
import su.pank.train2024.data.AddressRepository
import su.pank.train2024.data.DarkModeRepository
import su.pank.train2024.data.SupabaseRepository
import su.pank.train2024.data.onboardingseen.LocalOnBoardingSeenRepository
import su.pank.train2024.data.onboardingseen.OnBoardingSeenRepository

val dataModule = module {
    single<OnBoardingSeenRepository> {
        LocalOnBoardingSeenRepository()
    }
    single {
        SupabaseRepository.client
    }
    single { DarkModeRepository() }

    single { AddressRepository() }
}