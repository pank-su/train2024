package su.pank.train2024.data.di

import org.koin.dsl.module
import su.pank.train2024.data.onboardingseen.FakeOnBoardingSeenRepository
import su.pank.train2024.data.onboardingseen.OnBoardingSeenRepository

val fakedataModule = module {
    factory<OnBoardingSeenRepository> {
        FakeOnBoardingSeenRepository()
    }
}