package su.pank.train2024.data.model

import kotlinx.serialization.Serializable
import su.pank.train2024.ui.models.PackageDescription
import su.pank.train2024.ui.models.PositionDescription

@Serializable
data class PackageDetailsDTO(val origin: PositionDescription, val dests: List<PositionDescription>, val packageDescription: PackageDescription)