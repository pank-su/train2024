package su.pank.train2024.data.onboardingseen

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import su.pank.train2024.MainApplication

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

class LocalOnBoardingSeenRepository: OnBoardingSeenRepository {
    private val key = booleanPreferencesKey("isViewed")
    override val isViewed: Flow<Boolean> = MainApplication.appContext.dataStore.data.map { it[key] ?: false }
    override suspend fun toggleViewing() {
        MainApplication.appContext.dataStore.edit {
            it[key] = true
        }
    }
}