package su.pank.train2024.data.onboardingseen

import kotlinx.coroutines.flow.Flow

interface OnBoardingSeenRepository {
    val isViewed: Flow<Boolean>

    suspend fun toggleViewing()
}