package su.pank.train2024.data.model

import kotlinx.serialization.Serializable

@Serializable
data class AddressDTO(val country: String, val state: String, val city: String, val road: String)
