package su.pank.train2024.data.model

import kotlinx.serialization.Serializable

@Serializable
data class LatLng(val latitude: Double, val longitude: Double)
