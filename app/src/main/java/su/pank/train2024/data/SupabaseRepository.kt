package su.pank.train2024.data

import io.github.jan.supabase.coil.CoilIntegration
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.gotrue.Auth
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.storage.Storage

object SupabaseRepository {
    val client = createSupabaseClient(
        supabaseUrl = "https://haspgrbnqnckxfvhjatg.supabase.co",
        supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imhhc3BncmJucW5ja3hmdmhqYXRnIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTU3MTcwOTgsImV4cCI6MjAxMTI5MzA5OH0.4a1_uhDnWOXmXTODHVz2N7jbA7ch3l1-qzdZuKRztXQ"
    ) {
        install(Auth) {
            host = "app"
            scheme = "redis"
        }
        install(Postgrest)
        install(CoilIntegration)
        install(Storage)
    }
}