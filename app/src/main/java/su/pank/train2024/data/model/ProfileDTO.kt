package su.pank.train2024.data.model

import kotlinx.serialization.Serializable

@Serializable
data class ProfileDTO(val id: String, val fullname: String, val image: String, val balance: Int)
