package su.pank.train2024.data

import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import kotlinx.coroutines.flow.map
import su.pank.train2024.MainApplication
import su.pank.train2024.data.onboardingseen.dataStore

class DarkModeRepository {

    private val key = booleanPreferencesKey("darkMode")

    val isDark = MainApplication.appContext.dataStore.data.map {
        it[key] ?: false
    }

    suspend fun setDarkMode(enabled: Boolean) {
        MainApplication.appContext.dataStore.edit {
            it[key] = enabled
        }
    }
}