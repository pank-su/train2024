package su.pank.train2024.data.onboardingseen

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class FakeOnBoardingSeenRepository: OnBoardingSeenRepository {
    private val _isViewed = MutableStateFlow(false)

    override val isViewed: Flow<Boolean> = _isViewed.asStateFlow()

    override suspend fun toggleViewing() {
        _isViewed.value = false
    }
}