package su.pank.train2024

import androidx.compose.material3.Text
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsNotDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.isDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.delay

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.BeforeClass
import org.junit.Rule
import su.pank.train2024.ui.models.OnBoardInfo
import su.pank.train2024.ui.screen.onboard.OnBoardingScreen
import su.pank.train2024.ui.theme.Train2024Theme


import java.util.LinkedList
import java.util.Queue
import kotlin.random.Random

@RunWith(AndroidJUnit4::class)
class OnBoardingTest {
    @get:Rule
    val composeTestRule = createComposeRule()
    private val images = listOf(R.drawable.first, R.drawable.second, R.drawable.third)

    private val screens = buildList<OnBoardInfo> {
        repeat(100){
            add(OnBoardInfo(images.random(), title = "${Random.nextInt()} title", description = "${Random.nextInt()} desc"))
        }
    }


    @Test
    fun checkOnBoardingLogic() {
        val size = screens.size
        val normalLinkedList = LinkedList<OnBoardInfo>()
        normalLinkedList.addAll(screens)

        composeTestRule.setContent {
            val navController = rememberNavController()
            Train2024Theme {
                NavHost(navController = navController, startDestination = "OnBoardScreen"){
                    composable("OnBoardScreen"){
                        OnBoardingScreen(navController = navController, queue = normalLinkedList as Queue<OnBoardInfo>)
                    }
                    composable("Holder"){
                        Text(text = "It holder")
                    }
                }
            }
        }

        var currentPosition = 0

        while (currentPosition != size - 1){
            val currentScreen = screens[currentPosition]
            composeTestRule.onNodeWithContentDescription("Title").assertTextEquals(currentScreen.title)
            composeTestRule.onNodeWithContentDescription("Description").assertTextEquals(currentScreen.description)
            composeTestRule.onNodeWithText("Next").performClick()
            currentPosition++
        }
        composeTestRule.onNodeWithText("Next").assertIsNotDisplayed()
        //composeTestRule.onNodeWithText("Next").assertIsNotDisplayed()

        composeTestRule.onNodeWithText("Sign Up").assertIsDisplayed()
        composeTestRule.onNodeWithText("Sign Up").performClick()

        composeTestRule.onNodeWithText("It Holder").isDisplayed()


    }

    @Test
    fun checkSkip(){
        val normalLinkedList = LinkedList<OnBoardInfo>()
        normalLinkedList.addAll(screens)

        composeTestRule.setContent {

            val navController = rememberNavController()
            Train2024Theme {
                NavHost(navController = navController, startDestination = "OnBoardScreen"){
                    composable("OnBoardScreen"){
                        OnBoardingScreen(navController = navController, queue = normalLinkedList as Queue<OnBoardInfo>)
                    }
                    composable("Holder"){
                        Text(text = "It holder")
                    }
                }
            }
        }
        composeTestRule.onNodeWithText("Skip").performClick()
        composeTestRule.onNodeWithText("It Holder").isDisplayed()

    }
}