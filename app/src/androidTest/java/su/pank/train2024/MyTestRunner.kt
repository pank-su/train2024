package su.pank.train2024

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import org.junit.Test

class MyTestRunner: AndroidJUnitRunner() {
    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        return super.newApplication(cl, TestApplication::class.java.name, context)
    }
}